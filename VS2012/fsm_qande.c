#include <n_frame.h>
#include <fsm_qande.h>

/* 转换执行的外部函数声明 */
extern void IDLE_TO_Q(void);
extern void Q_TO_QE(void);
extern void QE_TO_IDLE(void);
extern void QE_TO_Q(void);
extern void Q_TO_IDLE(void);

/* 状态处理函数声明 */
void FSM_QandE_IDLE(NF_FSM* me, NF_Event event);
void FSM_QandE_Q_DOWN(NF_FSM* me, NF_Event event);
void FSM_QandE_QE_DOWN(NF_FSM* me, NF_Event event);

/* 状态机对象 */
NF_FSM FSM_QandE = {
    FSM_QandE_IDLE
};

/* IDLE状态处理函数 */
void FSM_QandE_IDLE(NF_FSM* me, NF_Event event)
{

    if (NF_FSM_NameIs(event.Name, "Q_DOWN"))
    {
        IDLE_TO_Q();
        NF_FSM_TRAN(FSM_QandE_Q_DOWN);
        return ;
    }

    if (NF_FSM_NameIs(event.Name, "test"))
    {
        NF_FSM_TRAN(FSM_QandE_IDLE);
        return ;
    }
}

/* Q_DOWN状态处理函数 */
void FSM_QandE_Q_DOWN(NF_FSM* me, NF_Event event)
{

    if (NF_FSM_NameIs(event.Name, "E_DOWN"))
    {
        Q_TO_QE();
        NF_FSM_TRAN(FSM_QandE_QE_DOWN);
        return ;
    }

    if (NF_FSM_NameIs(event.Name, "Q_UP"))
    {
        Q_TO_IDLE();
        NF_FSM_TRAN(FSM_QandE_IDLE);
        return ;
    }
}

/* QE_DOWN状态处理函数 */
void FSM_QandE_QE_DOWN(NF_FSM* me, NF_Event event)
{

    if (NF_FSM_NameIs(event.Name, "Q_UP"))
    {
        QE_TO_IDLE();
        NF_FSM_TRAN(FSM_QandE_IDLE);
        return ;
    }

    if (NF_FSM_NameIs(event.Name, "E_UP"))
    {
        QE_TO_Q();
        NF_FSM_TRAN(FSM_QandE_Q_DOWN);
        return ;
    }
}


